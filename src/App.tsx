import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Multiplier } from './Component/Multiplier';
import { Divider } from './Component/Divider';
import { Subtraction } from './Component/Subtraction';
import { Counter } from './Component/Counter';

function App() {
  return (
    <>
      <nav className="navbar navbar-light bg-primary ">
        <a className="navbar-brand text-white m-1" href="#">Assignment</a>
      </nav>
      <div className="container mt-2">
        <div className=''>

        </div>
        <div className='row'>
          <div className='col-sm-6'>
            <Counter />
          </div>
          <div className='col-sm-6'>
            <Divider />
          </div>
        </div>
        <div className='row mt-5'>
          <div className='col-sm-6'>
            <Multiplier />
          </div>
          <div className='col-sm-6'>
            <Subtraction />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
