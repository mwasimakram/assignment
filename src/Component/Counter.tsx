import { useEffect, useState } from "react"

export function Counter() {
    const [inputValue, setInputValue] = useState(0);
    const increment = () => {
        setInputValue(inputValue + 1);
        console.log("Increment", inputValue);
    }
    const decrement = () => {
        if (inputValue > 0) {
            setInputValue(inputValue - 1);
            console.log("decrement", inputValue)
        }
    }
    useEffect(() => {
        localStorage.setItem("value", JSON.stringify(inputValue));
        window.dispatchEvent(new Event("storage"));
    }, [inputValue])
    return (
        <div className="divBox">
            <h3>Counter</h3>
            <button className=" btn btn-primary" onClick={() => increment()}>Increment</button>
            <input className="m-2" type="number" placeholder="enter value" value={inputValue} onChange={() => { setInputValue(inputValue) }}></input>
            <button className="btn btn-secondary" onClick={() => decrement()}> Decrement</button>
        </div>
    )
}