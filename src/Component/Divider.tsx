import { useEffect, useState } from "react"
import { json } from "stream/consumers";
import { JsxEmit } from "typescript";

export function Divider(){
    const dividend =6;
    const [divisor,setDivisor] = useState(0);
    useEffect(()=>{
        function getStorageValue(event: any) {
            setDivisor(JSON.parse(localStorage.getItem('value')|| ''))
        }
        window.addEventListener("storage", getStorageValue);

    },[])
    return (
        <div className="divBox">
            <h3>Division</h3>
            {dividend} / {divisor}
            {
                divisor > 0 &&  <div className="col-sm-2 box">{dividend /divisor }</div> 

            }
        </div>
    )
}