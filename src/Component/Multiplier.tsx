import { useEffect, useState } from "react"

export function Multiplier() {
    const multiplier = -5;
    const [multiplicand, setMultiplicand] = useState(0);
    useEffect(() => {
        function getStorageValue(event: any) {
            setMultiplicand(JSON.parse(localStorage.getItem('value') || ''))
        }
        window.addEventListener("storage", getStorageValue);

    }, [])
    return (
        <div className="divBox">
            <h3>Multiplication</h3>
            {multiplier} * {multiplicand}
            <div className="col-sm-2 box">
                {multiplier * multiplicand}
            </div>
        </div>
    )
}