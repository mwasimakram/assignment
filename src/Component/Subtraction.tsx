import { useEffect, useState } from "react"

export function Subtraction() {
    const defaultValue = 4;
    const [value, setValue] = useState(0);
    useEffect(() => {
        function getStorageValue(event: any) {
            setValue(JSON.parse(localStorage.getItem('value') || ''))
        }
        window.addEventListener("storage", getStorageValue);

    }, [])
    return (
        <div className="divBox">
            <h3>Subtraction</h3>
            {defaultValue} - {value}
            <div className="col-sm-2 box">
                 {defaultValue - value}
            </div>
        </div>
    )
}